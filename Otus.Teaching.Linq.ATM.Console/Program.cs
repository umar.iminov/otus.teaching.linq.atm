﻿
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;


namespace Otus.Teaching.Linq.ATM.Console
{
    using System;
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            while (true)
            {
                Console.WriteLine("Выберите действие: login, listall, listsum, exit");
                string action = Console.ReadLine();
                switch (action)
                {
                    case "exit":
                        Console.WriteLine("Завершение работы приложения-банкомата...");
                        return;
                    case "login":
                        RunByLogin(atmManager);
                        break;
                    case "listall":
                        Console.WriteLine("Информация о всех поступлениях на счета пользователей:");
                        Console.WriteLine("***************************");
                        Console.WriteLine(atmManager.GetInfoIncome());
                        break;
                    case "listsum":
                       
                        Console.WriteLine("Информация об остатках на счетах всех пользователей. Введите сумму:");
                        try
                        {
                            Console.WriteLine(atmManager.GetInfoUserMoney(Convert.ToDecimal(Console.ReadLine())));
                        }
                        catch
                        {
                            Console.WriteLine("Некорректно введена сумма!");
                        }
                        break;
                    default:
                        Console.WriteLine("Неверный формат команды...");
                        break;
                }
                               
               
            }


        }
        static void RunByLogin(ATMManager atm)
        {
            Console.WriteLine("Введите логин:");
            string login = Console.ReadLine();
            Console.WriteLine("Введите пароль:");
            string pwd = Console.ReadLine();
            Console.WriteLine($"Информация по пользователю:{login}");
            Console.WriteLine("***************************");
            var user = atm.GetUserInfo(login, pwd);
            Console.WriteLine(user.ToString());
            Console.WriteLine($"Информация по счетам пользователя:{login}");
            Console.WriteLine("***************************");
            Console.WriteLine(atm.GetUsersAcc(user.Id));
            Console.WriteLine($"Информация по операциям пользователя:{login}");
            Console.WriteLine("***************************");
            Console.WriteLine(atm.GetUsersAccHistory(user.Id));
        }
        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}